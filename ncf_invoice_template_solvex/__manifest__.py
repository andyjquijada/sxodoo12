{
    'name': "NCF Invoice Template Solvex",
    'summary': """
    Este modulo sobre escribe el formato de las facturas adaptadas a la
    Norma General 06-2018 de la DGII, para incluir información adicional
    de la empresa.
    """,
    'author': "Andy Quijada",
    'license': 'LGPL-3',
    'category': 'Localization',
    'version': '12.0.2.1.0',
    'depends': ['web', 'ncf_invoice_template', 'purchase'],
    'data': ['report/report_invoice.xml',
             'report/purchase_order_templates_solvex.xml',
             'report/sale_report_templates_solvex.xml'],
}
